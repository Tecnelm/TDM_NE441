package tdm2;

import java.awt.Color;

import javax.swing.JFrame;

public class Exercice1 {

	public static void main(String[] args) throws Exception {
		Exercice1 c = new Exercice1();
        c.execute();
	}
	
	private void execute() throws Exception
    {
        JFrame frame = new JFrame("Chenillard");
        frame.setSize(300,300);

        //
        frame.getContentPane().setBackground(Color.GREEN);
        frame.setVisible(true);
        Thread.sleep(2000);


        //
        frame.getContentPane().setBackground(Color.RED);
        frame.setVisible(true);
        Thread.sleep(2000);


        frame.getContentPane().setBackground(Color.GREEN);
        frame.setVisible(true);
        Thread.sleep(2000);

        frame.dispose();
        
        /// crée une fenêtre d'une taille 300,300 
        /*
         * va ensuite mettre une couleur  et mettre a jour la fenêtre avec la fonction setVisible 
         * puis met l'exécution en pause pendant 2s refais la même chose avec la couleur rouge et verte 
         */

    }

}
