package tdm2;

import java.awt.Color;
import java.awt.Dimension;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

import javax.swing.JFrame;

public class Exercice3 {
	
	private final static int PORTA = 30000;
	private final static int PORTB = 30001;
	
	public static void main(String[] args) throws Exception {
		
		Thread server = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Exercice3 serveur = new Exercice3();
					serveur.serveur();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
		
		Thread client = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Exercice3 client = new Exercice3();
					client.client();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		server.start();
		client.start();
		
	}
	
	private void serveur() throws Exception
	{
		System.err.println("Creation server ...");
		InetSocketAddress adresse = new InetSocketAddress(PORTA);
		InetSocketAddress adresseSend = new InetSocketAddress("127.0.0.1",PORTB);
		DatagramSocket socket = new DatagramSocket(null);
		socket.bind(adresse);
		
		byte[] buffer = new byte[2048];
		
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		DatagramPacket packetSend;
		
		
		
		System.err.println("Creation frame 1 ");
		JFrame frame = new JFrame("Programme 1");
		frame.setSize(new Dimension(300,300));
		
		frame.getContentPane().setBackground(Color.red);
		frame.setVisible(true);
		
		String message; 
		byte[] send;
		int cnt  = 0 ;
		
		do
		{
			cnt++;
			System.err.println("Wait 1S prog 1");
			Thread.sleep(1000);
			frame.getContentPane().setBackground(Color.GREEN);
			frame.setVisible(true);
			
			send = "red".getBytes();
			packetSend= new DatagramPacket(send, send.length,adresseSend);
			socket.send(packetSend);
			socket.receive(packet);
			
			message = new String(buffer,packet.getOffset(),packet.getLength());

			if (message.equals("red"))
			{
				frame.getContentPane().setBackground(Color.red);
				frame.setVisible(true);
			}
				
		}while(cnt!= 10);
		
		socket.close();
		frame.dispose();
		
		
		
	}
	
	private void client() throws Exception
	{
		System.err.println("Creation client");
		InetSocketAddress adresse = new InetSocketAddress(PORTB);
		InetSocketAddress adresseSend = new InetSocketAddress("127.0.0.1",PORTA);
		DatagramSocket socket = new DatagramSocket(null);
		socket.bind(adresse);
		
		byte[] buffer = new byte[2048];
		
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		DatagramPacket packetSend;
		
		
		System.err.println("Creation client frame ");
		JFrame frame = new JFrame("Programme 2");
		frame.setSize(new Dimension(300,300));

		
		frame.getContentPane().setBackground(Color.GREEN);
		frame.setVisible(true);
		
		String message; 
		byte[] send;
		int cnt  = 0 ;
		
		do
		{
			System.err.println("Wait packet");
			cnt++;
			socket.receive(packet);
			message = new String(buffer,packet.getOffset(),packet.getLength());

			if (message.equals("red"))
			{
				frame.getContentPane().setBackground(Color.red);
				frame.setVisible(true);
			}	
			
			Thread.sleep(1000);
			
			send = "red".getBytes();
			packetSend= new DatagramPacket(send, send.length,adresseSend);
			socket.send(packetSend);
			
			frame.getContentPane().setBackground(Color.GREEN);
			frame.setVisible(true);
				
		}while(cnt!= 10);
		
		socket.close();
		frame.dispose();
		
	}
	
	private void sync(DatagramSocket socket,InetSocketAddress adress)
	{
	}
	

}
