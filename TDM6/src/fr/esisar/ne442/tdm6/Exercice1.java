package fr.esisar.ne442.tdm6;
import java.util.ArrayList;

public class Exercice1 extends Thread
{
    private Somme somme;

    public Exercice1(Somme somme)
    {
        this.somme = somme;
    }

    @Override
    public void run()
    {
        int res = 0;
        for (int i = 0; i < 1000; i++)
        {
            res= somme.somme(res, i);
        }
        System.out.println("La somme 1+2+3+4+...+998+999 est égale à :"+res);
    }


    public static void main(String[] args) throws InterruptedException
    {
        Somme somme = new Somme();
        Exercice1 c1 = new Exercice1(somme);
        Exercice1 c2 = new Exercice1(somme);
        
        Somme somme1 = new Somme();
        Somme somme2 = new Somme();
        Exercice1 c3 = new Exercice1(somme1);
        Exercice1 c4 = new Exercice1(somme2);

//        c1.start();
//        c2.start();
        
        c3.start();
        c4.start();
    }


    static  class Somme
    {

        int c;
        Object o = new Object();

        public synchronized int somme(int a, int b) 
        {
        	synchronized (o) {
        		c=a+b;
        		System.out.println("c="+c);
        		return c;				
			}
        }

    }

}