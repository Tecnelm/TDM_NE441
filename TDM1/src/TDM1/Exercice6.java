package TDM1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class Exercice6 {
	public static void main(String[] args) throws IOException {
		Exercice6 ex = new Exercice6();
		ex.jouer();
		
	}
	
	private void jouer() throws IOException
	{
		System.out.println("Demarrage Client ....");
		InetSocketAddress address = new InetSocketAddress("127.0.0.1",29000);
		DatagramSocket socket = new DatagramSocket();
		byte[] receive = new byte[2048];
		DatagramPacket packet = new DatagramPacket(receive,receive.length,address);
		DatagramPacket sendPacket ;
		String message;
		for(int i = 0 ; i <10;i++)
		{
			System.err.println("LANCEMENT PARTIE "+(i+1));
			
		
		byte[] send = "JOUER".getBytes();
		sendPacket= new DatagramPacket(send,send.length,address);
		socket.send(sendPacket);
		/// game start ping pong 
		do
		{
			socket.receive(packet);
			message = new String(receive,packet.getOffset(),packet.getLength());
			switch (message) {
			case "PING":  send = "PONG".getBytes();break;
			case "PONG": send = "PING".getBytes();break;
			default: send = "".getBytes();break;
			}
			
			
			if (!message.contains("GAGNE") && !message.contains("PERDU"))
			{
				sendPacket= new DatagramPacket(send,send.length,address);
				socket.send(sendPacket);				
			}
			
			System.err.println("Reception message : "+ message);
			System.err.println("Envoie message: " + new String(send));
			
			
			
		}while(!message.contains("GAGNE") && !message.contains("PERDU"));
		
		System.err.println("Partie "+(i+1)+" terminé vous avez: "+message);
		
		}
		
		socket.close();
		
	}

}
