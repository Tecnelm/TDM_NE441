package TDM1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class Exercice4 {
	
	public static void main(String[] args) throws IOException {
		Exercice4 ex = new Exercice4();
		ex.execute();
		
	}
	
	private void execute() throws IOException
	{
		System.out.println("Démarage serveur");
		
		DatagramSocket socket = new DatagramSocket(null);
		InetSocketAddress ad = new InetSocketAddress(3000);
		socket.bind(ad);
		
		
		byte [] receive = new byte[2048];
		DatagramPacket packet = new DatagramPacket(receive, receive.length);
		socket.receive(packet);
		String message = new String(receive,packet.getOffset(),packet.getLength());
		System.out.println("Message recu = "+message);
		socket.close();
		
	}

}
