package TDM1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.Scanner;

public class Exercice5 {

	public static void main(String[] args) throws IOException {
		Exercice5 ex = new Exercice5();
		Thread receive = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					ex.receive();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		Thread emit = new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				try {
					ex.emit();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		receive.start();
		emit.start();
		
		
		

	}
	
	private void receive() throws IOException
	{
		
		System.out.println("Démarage Serveur");
		InetSocketAddress add = new InetSocketAddress(3000);
		DatagramSocket socket= new DatagramSocket(null);
		socket.bind(add);
		
		
		byte [] buff = new byte[2048];
		DatagramPacket packet = new DatagramPacket(buff, buff.length);
		String message ;
		do
		{
			socket.receive(packet);
			message = new String(buff,packet.getOffset(),packet.getLength());
			System.out.println("Message = "+message);
			
		}while(!message.contains("stop"));
		socket.close();
		System.out.println("Fin de transmission serveur");
	}
	
	
	private void emit() throws IOException
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Démarage du client ... ");
		DatagramSocket so = new DatagramSocket();
		InetSocketAddress ard = new InetSocketAddress("127.0.0.1",3000);
		String message;
		DatagramPacket packet;
		do
		{
			message = scan.nextLine();
			byte [] buff = message.getBytes();
			packet = new DatagramPacket(buff, buff.length,ard);
			so.send(packet);
		}while(!message.contains("stop"));
		
		so.close();
		scan.close();
		System.out.println("Fin transmission client");
		
		
	}

}
