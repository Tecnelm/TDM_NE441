package fr.esisar.ne442.tmd5;

import java.util.ArrayList;

public class Exercice2  extends Thread{
	
	public double result = 0d;
	private long start;
	private long end;
	public final static int NBTHREAD = 8;
	public final static long INDEXPI= 1000000000;

	public static void main(String[] args) throws InterruptedException {
		ArrayList<Exercice2> exL = new ArrayList<Exercice2>();
		
		for (int i = 0; i <NBTHREAD ; i++ )
		{
			exL.add(new Exercice2((INDEXPI/NBTHREAD)*i, (INDEXPI/NBTHREAD)*(i+1)));
		}
		long start = System.currentTimeMillis();
		
		for(Exercice2 ex : exL)
			ex.start();
		for(Exercice2 ex: exL)
			ex.join();
		
		double result = 0;
		
		for(Exercice2 ex : exL)
			result+=ex.result;
		long stop = System.currentTimeMillis();
		
		System.err.println("Time Elapsed = "+(stop-start )+" ms");
		
		System.out.println(result* 4d);
		
		
	}
	@Override
	public void run()
	{
		this.calc(start, end);
	}
	public Exercice2(long start,long end)
	{
		this.start = start;
		this.end = end;
	}
	
	
	public void calc(long start,long end)
	{
		double result;
		for(long i = start; i < end ;i++)
		{
			
			result = 1d/((2d*(double)i)+1d) ;
			if (i%2 == 1)
			{
				result*= -1d;
			}
			
			this.result += result;
			
		}
	}
	
	
	

}
