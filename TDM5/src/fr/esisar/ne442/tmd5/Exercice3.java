package fr.esisar.ne442.tmd5;

import java.util.ArrayList;

public class Exercice3 extends Thread {

	public int id;
	private int time;
	public final static int NBTASK = 7;
	private ArrayList<Exercice3> await;

	public static void main(String[] args) {

		ArrayList<Exercice3> exL = new ArrayList<>();
		for (int i = 0; i < NBTASK; i++) {
			exL.add(new Exercice3(i + 1, (int) (Math.random() * 1000)));

		}
		
		int[] t1 = { 1 };
		exL.get(1).setAwait(exL, t1);
		exL.get(2).setAwait(exL, t1);
		exL.get(3).setAwait(exL, t1);

		int[] t2 = { 2, 3 };
		exL.get(4).setAwait(exL, t2);

		int[] t3 = { 4 };
		exL.get(5).setAwait(exL, t3);
		
		int[] t4 = { 5,6 };
		exL.get(6).setAwait(exL, t4);

		for(Exercice3 ex: exL)
			ex.start();
	}

	public Exercice3(int Id, int timeStop) {
		this.id = Id;
		this.time = timeStop;
		this.await = new ArrayList<>();

	}

	public void setAwait(ArrayList<Exercice3> ex, int[] awaitId) {

		for (int i : awaitId) {
			this.await.add(ex.get(i - 1));
		}
	}

	@Override
	public void run() {
		
		System.err.println("Thread: id : "+this.id+" :wait  Thread");
		for (Exercice3 ex : await) {
			try {
				ex.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.err.println("Thread: id : "+this.id+" : Start sleep");

		try {
			Thread.sleep(this.time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("Thread: id : "+this.id+" : End sleep");

	}

}
