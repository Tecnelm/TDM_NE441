package tdm3;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Exercice3 {
	
	private final static int PORT = 2000;
	
	public static void main(String[] args)throws Exception {
		
		 new Exercice3().execute();
	}
	
	private void execute() throws Exception
	{
		System.err.println("Demarage Server ...");
		
		ServerSocket server = new ServerSocket();
		server.bind(new InetSocketAddress(PORT));
		
		System.err.println("Server: Waiting client connection...");
		Socket client = server.accept();
		
		System.err.println("Server : New Client \n"
				+"IP: "+client.getInetAddress()+"\t Port: "+client.getPort());
		
		byte[] bufferR = new byte[2048];
		
		InputStream input;
		String message="";
		int lenBuf;
		do
		{
			input = client.getInputStream();
			lenBuf = input.read(bufferR);
			if (lenBuf != -1)
			{
				message = new String(bufferR,0,lenBuf);
				System.err.println("Server: Message receive = "+message);
			}
			
		}while(!message.contains(("STOP")) && lenBuf != -1);
		
		client.close();
		System.err.println("Server: Close client");
		server.close();
		System.err.println("Fermeture Server...");
	
	}

}
