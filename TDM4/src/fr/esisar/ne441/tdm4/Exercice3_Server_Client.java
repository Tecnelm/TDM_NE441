package fr.esisar.ne441.tdm4;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Exercice3_Server_Client extends Thread {

	private Socket so;
	private InputStream in;
	private OutputStream os;
	private int id;
	private String filePath;
	private int buffersize;

	public Exercice3_Server_Client(Socket so,String filepath, int id,int buffersize) throws IOException {
		this.so = so;
		this.in = so.getInputStream();
		this.os = so.getOutputStream();
		this.id = id;
		this.buffersize = buffersize;
		this.filePath=filepath;
		this.start();
	}

	@Override
	public void run() {
		System.err.println("Server Client: " + this.id + " Start");
		int bufferlen = 0;
		String message;
		StringBuilder builder = new StringBuilder();
		byte[] receiveBuffer = new byte[2048];
		char c;
		try {
			do {
				bufferlen = in.read(receiveBuffer);
				if (bufferlen != -1) {
					System.err.println("Server Client: " + this.id + ": reveive Data");
					message = new String(receiveBuffer, 0, bufferlen);
					System.err.println("Server Client: " + this.id + ": message = " + message);
					for (int i = 0; i < bufferlen; i++) {
						c = message.charAt(i);
						builder.append(c);
						if (c == ';') {
							System.err.println("Server Client: " + this.id + ": Request = " + builder.toString());

							this.executeRequest(builder.toString());
							builder.setLength(0);
						}
					}
				}

			} while (bufferlen != -1);
			System.err.println("Server Client: " + this.id + " Stop");


		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	private void sendData(String name) throws IOException {
		String filepath = this.filePath+"/"+name;
		
		File file = new File(filepath);
		FileInputStream in = new FileInputStream(file);
		long length = file.length();
		System.err.println("Server Client:" + this.id +  "send file = "+ this.filePath+"/"+name);
		
		byte[] buffer = new byte[this.buffersize];

		int len;
		System.err.println("Server Client: send message =" +"request:fileFound:size:"+length+";" );
		this.os.write(("request:fileFound:size:"+length+";").getBytes());
		System.err.println("Server Client:" + this.id +  "start sending file ");
		
		do {
			len = in.read(buffer);
			if (len !=-1)
			{
				this.os.write(buffer,0,len);
			}
		}while(len != -1);
		
		System.err.println("Server Client:" + this.id + " end sending file");
		this.so.close();
		

	}

	private void executeRequest(String message){
		String[] request;
		request = message.split(":");
		switch (request[0]) {
		case "AskFile":
			try
			{
				this.sendData(request[1].substring(0,request[1].length()-1));
				
			} catch (Exception e) {
				try {
					this.os.write(("request:fileNotFound:size:"+-1+";").getBytes());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			break;

		default:
			break;
		}

	}

}
