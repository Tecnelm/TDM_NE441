package fr.esisar.ne441.tdm4;

import java.awt.BorderLayout;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

public class Exercice3Client {

	private String destPath;
	private Socket socket;
	private int bufferSize;
	private long fileSize;
	private ProgressBar bar;

	public Exercice3Client(String destPath, int bufferSize) {
		this.destPath = destPath;
		this.bufferSize = bufferSize;

	}

	public void connexion(String IP, int port) throws Exception {
		System.err.println("Creation Client ....");
		Socket socket = new Socket();
		InetSocketAddress address = new InetSocketAddress(IP, port);
		socket.connect(address);

		this.socket = socket;

	}

	public void disconnect() throws IOException {
		this.socket.close();
	}

	public void askFile(String fileName) throws Exception {

		System.err.println("Client: Ask file = " + fileName);
		byte[] send = ("AskFile:" + fileName + ";").getBytes();

		OutputStream os = socket.getOutputStream();
		os.write(send);

		InputStream in = socket.getInputStream();

		byte[] buffer = new byte[bufferSize];
		int lenBuffer;
		FileOutputStream fileout = new FileOutputStream(this.destPath + "/" + fileName + "cpy");
		this.fileSize = this.getFileSize();
		StringBuilder sb = new StringBuilder();
		String message;
		char c;

		if (fileSize != -1) {
			this.bar = new ProgressBar(this.fileSize);
			long start = System.currentTimeMillis();
			do {
				lenBuffer = in.read(buffer);
				if (lenBuffer != -1) {
					fileout.write(buffer, 0, lenBuffer);
					
					//this.progressPercentage(this.bar.getProgressBar().getValue() + lenBuffer, this.fileSize);
					this.bar.getProgressBar().setValue(this.bar.getProgressBar().getValue() + lenBuffer);
				}
			} while (lenBuffer != -1);
			long stop = System.currentTimeMillis();
			System.err.println("Client: End Transmission....");
			System.err.println("Time Elapsed = " + (stop - start) + " ms");
			this.bar.dispose();
			JOptionPane.showMessageDialog(null,
				    "Download End : Time Elapsed =" + (stop - start) + " ms");
		} else {
			System.err.println("Client: Error Downloading file ...");
		}
		fileout.close();
		this.socket.close();
		

	}

	private long getFileSize() throws IOException {
		System.err.println("Client: Start Reading fileSize ... ");
		int bufferLen;
		StringBuilder sb = new StringBuilder();
		InputStream in = this.socket.getInputStream();

		String message;
		byte[] buffer = new byte[this.bufferSize];
		char c;

		do {
			bufferLen = in.read(buffer);

			if (bufferLen != -1) {
				message = new String(buffer, 0, bufferLen);
				for (int i = 0; i < bufferLen; i++) {
					c = message.charAt(i);
					if (c == ';') {
						String[] request = sb.toString().split(":");
						System.err.println("Client: Request end, request = " + sb.toString());
						if (request[1].equals("fileFound")) {
							Integer size = Integer.parseInt(request[3]);
							System.err.println("Client: FileSize = " + size);
							return size;
						}
						else
						{
							System.err.println("Client: File Not Found");
							return -1;
						}
					}
					sb.append(c);
				}
			}

		} while (bufferLen != -1);

		System.err.println("Client: Error ReadSize");

		return -1;
	}

	class ProgressBar extends JFrame {
		private JProgressBar bar;

		public ProgressBar(long max){
			this.setSize(300, 80);
			this.setTitle("*** File Download ***");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setLocationRelativeTo(null);
			bar = new JProgressBar();
			bar.setMaximum((int)max);
			bar.setMinimum(0);
			bar.setStringPainted(true);
			this.getContentPane().add(bar, BorderLayout.CENTER);
			this.setVisible(true);
		}

		public JProgressBar getProgressBar() {
			return this.bar;
		}
	}
	
	private static void progressPercentage(long remain, long total) {
	    if (remain > total) {
	        throw new IllegalArgumentException();
	    }
	    int maxBareSize = 10; // 10unit for 100%
	    int remainProcent = (int) (((100 * remain) / total) / maxBareSize);
	    char defaultChar = '-';
	    String icon = "*";
	    String bare = new String(new char[maxBareSize]).replace('\0', defaultChar) + "]";
	    StringBuilder bareDone = new StringBuilder();
	    bareDone.append("[");
	    for (int i = 0; i < remainProcent; i++) {
	        bareDone.append(icon);
	    }
	    String bareRemain = bare.substring(remainProcent, bare.length());
	    System.out.print("\r" + bareDone + bareRemain + " " + remainProcent * 10 + "%");
	    if (remain == total) {
	        System.out.print("\n");
	    }
	}

}
