package fr.esisar.ne441.tdm4;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Exercice3Server {

	private ServerSocket socket;
	private final static int BUFFERSIZE = 10000;
	private String filepath;

	public static void main(String[] args) throws Exception {
		Thread client= new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					Exercice3Client client = new Exercice3Client("/home/clement", 2048);
					client.connexion("127.0.0.1", 3000);
					client.askFile("file1");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		client.start();
		
		Exercice3Server server = new Exercice3Server();
		server.create(3000,"/home/clement");
		server.listen();
		

	}

	public void create(int port,String filePath) throws Exception {
		System.err.println("Creating server on port :" + port);
		this.socket = new ServerSocket();
		InetSocketAddress address = new InetSocketAddress(port);
		socket.bind(address);
		this.filepath=filePath;

	}

	public void listen() throws IOException {
		System.err.println("Server: Start listening connexion ....");
		
		while(true)
		{
			System.err.println("Waiting new Client");
			Socket socketConnexion = socket.accept();
			
			System.err.println("Server: New connection: ");
			System.err.println("Server: IP: "+socketConnexion.getInetAddress());
			System.err.println("Server: Port: "+socketConnexion.getPort());
			
			new Exercice3_Server_Client(socketConnexion,this.filepath, 0, this.BUFFERSIZE);
		}

	}

}
