package fr.esisar.ne441.tdm4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;


public class Exercice1 {
	private int bufferSize= 1024;
	

	public static void main(String[] args) throws IOException {
		Exercice1 ex = new Exercice1();
		ex.copy("/home/clement/file1", "/home/clement/testNEcpy");

	}

	public void copy(String pathSrc, String pathDest) throws IOException {
		
		
		
		System.err.println("Opening File ....");
		FileInputStream fileInput = new FileInputStream(pathSrc);
		FileOutputStream fileOutput = new FileOutputStream(pathDest, false);

		System.err.println("File Open Success");
		byte[] dataIn = new byte[bufferSize];
		int bufferlen;

		long start = System.currentTimeMillis();
		System.err.println("Starting Copy file .....");
		do {
			bufferlen = fileInput.read(dataIn);
			if (bufferlen != -1)
				fileOutput.write(dataIn, 0, bufferlen);

		} while (bufferlen != -1);

		long stop = System.currentTimeMillis();
		System.err.println("End copy file ....");
		System.err.println("Elapsed Time = " + (stop - start) + " ms");

		fileInput.close();
		fileOutput.close();

	}
}
