package fr.esisar.ne442.tdmRevision;

import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;

public class Client_Scolarite {
	
	private String address;
	private String RMIName;
	
	public Client_Scolarite(String address,String RMIName) {
		this.address = address;
		this.RMIName = RMIName;
	}
	
	
	public static void main(String[] args) throws RemoteException {
		
		/// TODO SEcurity SYSTEM.GERSECURITY MANAGRer
		Client_Scolarite client = new Client_Scolarite("127.0.1.1", "Moyenne");
		
		Etudiant hugo = new Etudiant(0, "Dougy", "Hugo", 21, 2023);
		client.calculerMoyenne(hugo);
		
		System.out.println(hugo);
		


		
	}


	public void  calculerMoyenne( Etudiant etu)
	{
		
		try
		{
			StringBuilder builder = new StringBuilder();
			builder.append(this.address);
			builder.append("/");
			builder.append(this.RMIName);
			
			System.err.println("Calcul moyenne Etudiant: "+etu.getFirstName()+" "+etu.getName());
			System.err.println("Connection au serveur RMI : "+ builder.toString());
			
			Remote r = Naming.lookup(builder.toString());
			
			if(r instanceof Calculer)
			{
				System.err.println("Appel de la méthode calculMoyenne");
				((Calculer) r).calculMoyenneGenerale(etu);
				System.err.println("Fin calcul moyenne");
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	public String getAddress() {
		return address;
	}


	public String getRMIName() {
		return RMIName;
	}
	

}
