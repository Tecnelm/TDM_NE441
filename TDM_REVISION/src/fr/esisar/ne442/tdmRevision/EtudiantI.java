package fr.esisar.ne442.tdmRevision;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface EtudiantI extends Remote{
	public void setMoyenne(int moyenne) throws RemoteException;
}
