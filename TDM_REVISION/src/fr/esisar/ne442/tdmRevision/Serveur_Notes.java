package fr.esisar.ne442.tdmRevision;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Random;

public class Serveur_Notes extends UnicastRemoteObject implements Calculer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2841168658457011438L;

	@Override
	public void calculMoyenneGenerale(EtudiantI etu) throws RemoteException {
		ArrayList<Integer> note = this.randomNote(20);
		int moyenne = 0;
		for (Integer i : note) {
			moyenne += i;
		}
		moyenne /= note.size();
		System.err.println(moyenne);
		etu.setMoyenne(moyenne);
	}

	public Serveur_Notes(String RMIName) throws RemoteException, UnknownHostException {
		System.err.println("Creation du Serveur RMI");
		// instanciation d'un objet Server

		// Création du registry
		Registry registry = LocateRegistry.createRegistry(1099);

		// Appel de la méthode rebind pour enregistrer l'object serveur
		String url = InetAddress.getLocalHost().getHostAddress() + "/"+RMIName;
		System.err.println("Enregistrement RMI à l'adress " + url);
		registry.rebind(url,this);
		System.err.println("Server Ready!");
		
	}

	private ArrayList<Integer> randomNote(int nbNote) {
		ArrayList<Integer> noteL = new ArrayList<Integer>();
		for (int i = 0; i < nbNote; i++) {
			noteL.add(new Random().nextInt(21));
		}
		return noteL;
	}
	public static void main(String[] args) throws RemoteException, UnknownHostException {
		new Serveur_Notes("Moyenne");
	}

}
